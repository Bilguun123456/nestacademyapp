import React from 'react';
import { SafeAreaView } from 'react-native';
import { Stack, Queue, Box, Text } from '../../components';

export const TestScreen = () => {
    return (
        <SafeAreaView style={{flex: 1}}>
            <Box flex={1}>
                <Stack size={5} role={'caution200'}>
                    <Text type={"title1"}>Нэг түмэн инженер</Text>
                    <Text type={'title2'}>Нэг түмэн инженер</Text>
                    <Text type={'title3'}>Нэг түмэн инженер</Text>
                    <Text type={'headline'}>Нэг түмэн инженер</Text>
                    <Text type={'body'}>Нэг түмэн инженер</Text>
                    <Text type={'callout'}>Нэг түмэн инженер</Text>
                    <Queue size={3} alignItems={'center'} justifyContent={'flex-end'}>
                        <Text type={'subheading'}>Нэг түмэн инженер</Text>
                        <Text type={'caption1'}>Нэг түмэн инженер</Text>
                        <Text type={'caption2'}>Нэг түмэн инженер</Text>
                    </Queue>
                    <Text numberOfLines={4} type={'body'}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eros dui, pellentesque eget condimentum sed, interdum non lectus. Proin elementum risus elit, eget tempor lorem viverra vitae</Text>
                    <Text width={'50%'} numberOfLines={2} type={'body'}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eros dui, pellentesque eget condimentum sed, interdum non lectus. Proin elementum risus elit, eget tempor lorem viverra vitae</Text>
                </Stack>
                <Box flex={1} flexDirection={'row'}>
                    <Box flex={1} role={'primary100'}></Box>
                    <Box width={150} role={'primary200'}></Box>
                    <Box flex={2} role={'primary300'}></Box>
                </Box>
            </Box>
        </SafeAreaView>
    );
};
