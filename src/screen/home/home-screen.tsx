import React from 'react';
import { SafeAreaView } from 'react-native';
import { Padding, Text } from '../../components';

export const HomeScreen = () => {
    return (
        <SafeAreaView style={{ backgroundColor: '#FFFFFF' }}>
            <Padding size={[5, 10, 5, 10]}>
                <Text bold type={'largeTitle'}>Welcome to Nest APP</Text>
            </Padding>
        </SafeAreaView>
    );
};
