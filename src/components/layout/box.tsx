import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useTheme } from '../theme-provider';
import { colorType } from '../types';

export const Box: React.FC<BoxType> = (props) => {
    const { colors } = useTheme();
    const { flex = 0, height, width, flexDirection, flexWrap, justifyContent, alignItems, position, children, top, bottom, left, right, zIndex, role } = props;
    const styles = StyleSheet.create({
        flexDimension: {
            flex: flex || 1,
        },
        fixedDimension: {
            width: width || '100%',
            height: height || '100%',
        },
        flexBox: {
            display: 'flex',
            flexDirection: flexDirection || 'column',
            flexWrap: flexWrap || 'nowrap',
            justifyContent: justifyContent || 'flex-start',
            alignItems: alignItems || 'stretch',
        },
        position: {
            position: position || 'relative',
            top,
            bottom,
            left,
            right,
            zIndex
        },
        background: {
            backgroundColor: role ? colors[role] : 'transparent',
        }
    });

    return (
        <View
            style={[
                flex ? styles.flexDimension : styles.fixedDimension,
                styles.flexBox,
                styles.position,
                styles.background
            ]}
        >
            {children}
        </View>
    )
};

type BoxType = {
    flex?: number;
    flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
    flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
    justifyContent?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around' | 'space-evenly';
    alignItems?: 'stretch' | 'flex-start' | 'flex-end' | 'center' | 'baseline';
    position?: 'absolute' | 'relative';
    top?: string | number;
    bottom?: string | number;
    left?: string | number;
    right?: string | number;
    zIndex?: number;
    role?: colorType;
    width?: string | number;
    height?: string | number;
    children?: JSX.Element | JSX.Element[] | string;
};