import React from 'react';
import styled from 'styled-components/native';

const Size = { L: '128px', M: '64px', S: '40px', XS: '24px' }

type Avatart = {
    name?: string;
    lastname?: string;
    url?: string;
    size: 'L' | 'M' | 'S' | 'XS';
}

export const Avatar: React.FC<Avatart> = ({ name='Bilguun', lastname='Tsolmon', url, size }) => {

    type Cont = {
        border: boolean;
    }
    const Container = styled.View<Cont>`
        width: ${Size[size]};
        height: ${Size[size]};
        background-color: ${url ? '#FFF' : '#00DCF0'};
        border-radius: 100px;
        border-width: ${props => props.border ? '4px' : '0px'};
        border-color: #FAFAFA;
        justify-content: center;
        align-items: center;
    `

    const Image = styled.Image`
        width: ${Size[size]};
        height: ${Size[size]};
        border-radius: 100px;
    `

    const Text = styled.Text`
        font-style: normal;
        font-weight: bold;
        font-size: ${size === 'L' ? '34px' : size === 'M' ? '22px' : size === 'S' ? '12px' : 0};
    `

    return (
        <Container border={size!=='XS'}>
            {url ? <Image source={{uri: url}}/> : <Text>{name[0]}{lastname[0]}</Text>}
        </Container>
    );
};