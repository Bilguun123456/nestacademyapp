import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useTheme } from '../theme-provider';
import { fibonacci } from '../../utils';
import _ from 'lodash';

export const Spacing: React.FC<BoxType> = (props) => {
    const { baseSpace } = useTheme();
    const { m, mh, mv, ml, mr, mt, mb, p, ph, pv, pl, pr, pt, pb, children } = props;
    const styles = StyleSheet.create({
        margin: {
            margin: _.isNumber(m) ? fibonacci(m) * baseSpace : p,
            marginHorizontal: _.isNumber(mh) ? fibonacci(mh) * baseSpace : mh,
            marginVertical: _.isNumber(mv) ? fibonacci(mv) * baseSpace : mv,
            marginLeft: _.isNumber(ml) ? fibonacci(ml) * baseSpace : ml,
            marginRight: _.isNumber(mr) ? fibonacci(mr) * baseSpace : mr,
            marginTop: _.isNumber(mt) ? fibonacci(mt) * baseSpace : mt,
            marginBottom: _.isNumber(mb) ? fibonacci(mb) * baseSpace : mb,
        },
        padding: {
            padding: _.isNumber(p) ? fibonacci(p) * baseSpace : p,
            paddingHorizontal: _.isNumber(ph) ? fibonacci(ph) * baseSpace : ph,
            paddingVertical: _.isNumber(pv) ? fibonacci(pv) * baseSpace : pv,
            paddingLeft: _.isNumber(pl) ? fibonacci(pl) * baseSpace : pl,
            paddingRight: _.isNumber(pr) ? fibonacci(pr) * baseSpace : pr,
            paddingTop: _.isNumber(pt) ? fibonacci(pt) * baseSpace : pt,
            paddingBottom: _.isNumber(pb) ? fibonacci(pb) * baseSpace : pb,
        }
    });

    return (
        <View style={[styles.margin, styles.padding]} >
            {children}
        </View>
    )
};

type BoxType = {
    m?: string | number;
    mh?: string | number;
    mv?: string | number;
    ml?: string | number;
    mr?: string | number;
    mt?: string | number;
    mb?: string | number;
    p?: string | number;
    ph?: string | number;
    pv?: string | number;
    pl?: string | number;
    pr?: string | number;
    pt?: string | number;
    pb?: string | number;
    children?: JSX.Element | JSX.Element[] | string;
};